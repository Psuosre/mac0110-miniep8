function troca(v, i, j)
        aux = v[i]
        v[i] = v[j]
        v[j] = aux
end


function insercao(v)
        tam = length(v)
        for i in 2:tam
                j = i
                while j > 1
                        if v[j] < v[j - 1]
                                troca(v, j, j - 1)
                        else
                                break
                        end
                        j = j - 1
                end
        end
        return v
end

function compareByValue(s1,s2)
        digits = []
        push!(digits,separaDigito(s1))
        push!(digits,separaDigito(s2))
        if comparaDigito(digits[1], digits[2])
                return true
        else
                return false
        end
end

function separaDigito(x)
        text = x
        if length(text) == 3
                return text[1] * text[2]
        end
        if length(text) == 2
                return text[1]
        end
end

function comparaDigito(x,y)
        if (letraAvaluada(x)) < (letraAvaluada(y))
                return true
        else
                return false
        end
end

function letraAvaluada(x)
        if typeof(x) == Char
                x = string(x)
        end
        if typeof(x) == String
                if x == "A"
                        return 14
                end
                if x == "K"
                        return 13
                end
                if x == "Q"
                        return 12
                end
                if x == "J"
                        return 11
                end
                return parse(Int,x)
        end
end

function insercaoByValue(v)
        tam = length(v)
        for i in 2:tam
                j = i
                while j > 1
                        if compareByValue(v[j],v[j - 1])
                                troca(v, j, j - 1)
                        else
                                break
                        end
                        j = j - 1
                end
        end
        return v
end

using Test

function testCompareByValue()
        println("Início dos teste: compareByValue")
        @test compareByValue("10♠", "10♥") == false
        @test compareByValue("K♥", "10♥") == false
        @test compareByValue("2♠", "A♠") == true
        @test insercaoByValue(["10♥", "10♦", "K♠", "A♠", "J♠", "A♠"]) == ["10♥","10♦","J♠","K♠","A♠","A♠"]
        println("Fim dos testes: compareByValue")
end

function compareBySuit(s1,s2)
        naipes = []
        push!(naipes, separaNaipes(s1))
        push!(naipes, separaNaipes(s2))
        if comparaNaipe(naipes[1], naipes[2])
                return true
        else
                return false
        end
end

function separaNaipes(x)
        if length(x) == 3
                return x[3]
        end
        if length(x) == 2
                return x[2]
        end
end

function comparaNaipe(x,y)
        if naipeAvaluado(x) < naipeAvaluado(y)
                return true
        else
                return false
        end
end

function naipeAvaluado(x)
        if typeof(x) == Char
                x = string(x)
        end
        if typeof(x) == String
                if x == "♦"
                        return 1
                end
                if x == "♠"
                        return 2
                end
                if x == "♥"
                        return 3
                end
                if x == "♣"
                        return 4
                end
        end
end

function testCompareBySuit()
        println("Início dos teste: compareBySuit")
        @test compareBySuit("10♠", "10♥") == true
        @test compareBySuit("K♥", "10♥") == false
        @test compareBySuit("2♠", "A♠") == false
        println("Fim dos testes: compareBySuit")
end

function compareByValueAndSuit(x,y)
        if compareBySuit(x,y)
                return true
        else
                if compareByValue(x,y)
                        return true
                else
                        return false
                end
        end
end

function insercaoByValueAndSuit(v)
        tam = length(v)
        for i in 2:tam
                j = i
                while j > 1
                        if compareByValueAndSuit(v[j],v[j - 1])
                                troca(v, j, j - 1)
                        else
                                break
                        end
                        j = j - 1
                end
        end
        return v
end

function testCompareByValueAndSuit()
        println("Início dos teste: compareByValueAndSuit")
        @test compareByValueAndSuit("2♠", "A♠") == true
        @test compareByValueAndSuit("K♥", "10♥") == false
        @test compareByValueAndSuit("10♠", "10♥") == true
        @test compareByValueAndSuit("A♠", "2♥") == true
        @test insercaoByValueAndSuit(["10♥", "10♦", "K♠", "A♠", "J♠", "A♠"]) == ["10♦","J♠","K♠","A♠","A♠","10♥"]
        println("Fim dos testes: compareByValueAndSuit")
end

testCompareByValue()
testCompareBySuit()
testCompareByValueAndSuit()
